const path = require('path');
const BundleTracker = require('webpack-bundle-tracker');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: __dirname,

  entry: {
    main: [
      'babel-polyfill',
      './frontend/js/index'
    ],
  },

  plugins: [
    new BundleTracker({ filename: './webpack-stats.json' }),
    new HtmlWebpackPlugin({
      template: __dirname + '/frontend/js/index.html',
      filename: 'index.html',
      inject: 'body',
    }),
  ],

  output: {
    path: path.resolve('./frontend/bundles/'),
    filename: "[name]-[hash].js",
  },

  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader', // to transform JSX into JS
    }, {
      test: /\.(s*)css$/,
      loaders: [
        'style-loader',
        'css-loader',
        'sass-loader',
      ]
    }, {
      test: /\.(svg)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 8192, // Convert images < 8kb to base64 strings
          name: 'images/[hash]-[name].[ext]',
          fallback: 'url-loader',
        },
      }]
    }, {
      test: /\.(png|jp(e*)g)$/,
      use: [{
        loader: 'file-loader',
        options: {
          publicPath: 'static/bundles/',
          name: 'images/[hash]-[name].[ext]',
        },
      }]
    }, {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader'],
      }
    ],
  },

  resolve: {
    modules: ['node_modules', 'bower_components'],
    extensions: ['.js', '.jsx'],
  },
};
