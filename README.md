Softplan Challenge
===================

## Configuração

### Requerimentos

1. Docker operacional
2. NodeJs e NPM operacional

### Como rodar

1. Rode a imagem docker base de dados fornecida pelo teste: `docker run -d -p 5432:5432 justiandre/test-temp-pj-spec-gestao-processos-db:latest`
2. Navegue até a pasta `backend` e construa a imagem: `docker build -t softplan-backend .`
3. Rode a imagem do `backend`: `docker run -d -p 7070:7070 softplan-backend`
4. Navegue até a pasta do `frontend` e rode: `npm install` e depois `npm run start`
5. A aplicação estará disponível em: `http://localhost:3000/`

#### Observações

1. O `backend` ficará disponível na porta: `7070`

## Observações do Teste

1. Foi realizada a modelagem apenas da entidade responsável dada a necessidade do teste.
2. Não foi realizado o teste de validação fornecido pelo teste.
3. Não foi implementado os erros de NOT FOUND como descrito no teste para as ações de edição e deleção.
4. Devido ao tempo para execução do teste, para responsividade foi utilizado fontes fixas `px`, o que faz com que a fonte fique pequena em mobile e precise ser ajustada para cada versão.
5. Alguns elementos de terceiros (como o Date Picker) não ficaram encaixados na tela e nem com dimensão boa para usar em mobile. Optei por deixar assim pelo meu tempo disponível.

## O que poderia melhorar

1. Realizar o teste de validação fornecido pelo teste, paraque todos os casos passem com exatidão.
2. Implementar os erros de NOT FOUND como descrito no teste para as ações de edição e deleção.
3. Utilizar fontes que sejam relativas ao tamanho da tela (DPI).
4. Encaixar elementos de terceiros na tela para a estrutura ficar visualmente agradável.
5. Melhorar as cores e disposição de elementos para um formato mais moderno e agradável.
6. Modularização: há diversos elementos que poderiam ser transformados em componentes para diminuir código repetido.
7. Uso de variáveis: transformar as cores em variáveis do SCSS apra não ficar reperindo elas, o que causaria problemas em um refactor.
8. Atalhos: como já deixar elementos em highlight para o usuário sair digitando. Enter realizar consulta, edição ou cadastro.
