import storage from 'redux-persist/es/storage';
import { persistReducer, persistStore } from 'redux-persist';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import { createFilter } from 'redux-persist-transform-filter';
import createBrowserHistory from 'history/createBrowserHistory';

import rootReducer from './root-reducer';

const persistedFilter = createFilter('auth', ['']);
const reducer = persistReducer(
  {
    key: 'vault-teck',
    storage: storage,
    whitelist: [''],
    transforms: [persistedFilter]
  },
  rootReducer);

const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(createBrowserHistory)),
    /* eslint-disable no-undef */
    process.env.NODE_ENV !== 'production' && typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : (f) => f
  )
);

persistStore(store);

export default store;