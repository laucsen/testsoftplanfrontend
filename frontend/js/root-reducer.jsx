import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

// --------
// Reducers
import responsible from './responsible/reducer';
// --------

/**
 * Reducers
 */
export default combineReducers({
  responsible,
  //
  routing,
});
