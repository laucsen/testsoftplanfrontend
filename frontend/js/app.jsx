import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import './app.scss';

/**
 * Main app that wrapp all application.
 */
class App extends Component {
  static propTypes = {
    children: PropTypes.object,
  };

  render() {
    return (
      <div style={{
        height: '100%',
        overflowY: 'scroll',
        position: 'absolute',
        top: '0px',
        left: 0,
        right: 0,
        bottom: 0,
      }}>
        <div>
          {this.props.children}
        </div>
        <ToastContainer />
      </div>
    );
  }
}

export default App;
