import { handleActions } from 'redux-actions';
import {
  loadResponsibleSucceeded,
  getResponsibleSucceeded,
  editResponsibleSucceeded,
  nextPageAction,
  previousPageAction,
  goToPageAction,
} from './actions';

const defaultState = {
  records: [],
  recordsCount: -1,
  responsible: null,
  pageSize: 3,
  page: 0,
};

export default handleActions({
  [loadResponsibleSucceeded]: (state, { payload }) => {
    return {
      ...state,
      records: payload.records,
      recordsCount: payload.records_number,
    };
  },
  [getResponsibleSucceeded]: (state, { payload }) => {
    return {
      ...state,
      responsible: payload,
    };
  },
  [editResponsibleSucceeded]: (state) => {
    return {
      ...state,
      responsible: null,
    };
  },
  [nextPageAction]: (state) => {
    const pages = Math.ceil(state.recordsCount / state.pageSize);
    const next = state.page + 1;
    return {
      ...state,
      page: (next < pages) ? next : state.page,
    };
  },
  [previousPageAction]: (state) => {
    const previous = state.page - 1;
    return {
      ...state,
      page: (previous >= 0) ? previous : state.page,
    };
  },
  [goToPageAction]: (state, { payload }) => {
    return {
      ...state,
      page: payload,
    };
  },
}, defaultState);
