import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './index.scss';

class Header extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
  };

  render() {
    const { mobile } = this.props;

    return (
      <div className={`header-area ${mobile ? 'header-area-mobile' : 'header-area-desktop'}`}>
        <div className={`header-logo soft-text-large ${mobile ? 'header-logo-mobile' : ''}`}>LOGO</div>
        {!mobile ? <div className={'responsible-header-item soft-text-normal'}>Responsáveis</div> : null}
      </div>
    );
  }
}

export default Header;
