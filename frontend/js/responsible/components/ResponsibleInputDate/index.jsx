import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import pt from 'date-fns/locale/pt';

import 'react-datepicker/dist/react-datepicker.css';
import './index.scss';

registerLocale('pt', pt);
setDefaultLocale(pt);

class ResponsibleInputDate extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
    caption: PropTypes.string,
    value: PropTypes.any,
    onChange: PropTypes.func,
  };

  render() {
    const { caption, value, onChange, mobile } = this.props;

    return (
      <div className={'responsible-detail-input-item'}>
        <div className={`soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}>{caption}</div>
        <div className={'responsible-detail-block-input-area'}>
          <DatePicker
            className={`responsible-detail-block-input-date soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}
            selected={value}
            registerLocale={pt}
            setDefaultLocale={'pt'}
            dateFormat={'dd/MM/yyyy'}
            onChange={onChange} />
        </div>
      </div>
    );
  }
}

export default ResponsibleInputDate;
