import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './index.scss';

class ResponsibleInput extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
    caption: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
  };

  render() {
    const { caption, value, onChange, mobile } = this.props;

    return (
      <div className={'responsible-detail-input-item'}>
        <div className={`soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}>{caption}</div>
        <div className={'responsible-detail-block-input-area'}>
          <input className={`responsible-detail-block-input soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}
                 type="text"
                 value={value}
                 onChange={onChange} />
        </div>
      </div>
    );
  }
}

export default ResponsibleInput;
