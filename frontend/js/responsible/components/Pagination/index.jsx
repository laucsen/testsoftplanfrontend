import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  loadResponsibles,
  nextPage,
  previousPage,
  goToPage,
} from '../../actions';

import './index.scss';

class Pagination extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
    pageSize: PropTypes.number,
    page: PropTypes.number,
    recordsCount: PropTypes.number,
    loadResponsibles: PropTypes.func,
    nextPage: PropTypes.func,
    previousPage: PropTypes.func,
    goToPage: PropTypes.func,
  };

  nextPage() {
    const { pageSize, page } = this.props;
    this
      .props
      .nextPage()
      .then(this.props.loadResponsibles(pageSize, page + 1));
  }

  previousPage() {
    const { pageSize, page } = this.props;
    this
      .props
      .previousPage()
      .then(this.props.loadResponsibles(pageSize, page - 1));
  }

  goToPage(page) {
    const { pageSize } = this.props;
    this
      .props
      .goToPage(page)
      .then(this.props.loadResponsibles(pageSize, page));
  }

  getBlocks() {
    const { pageSize, recordsCount } = this.props;

    const pages = Math.ceil(recordsCount / pageSize);

    const pagesElements = [];
    for (let i = 0; i < pages; i++) {
      pagesElements.push(i);
    }

    return (
      <div>
        {pagesElements.map((item, index) => {
          return (
            <button
              className="soft-text-normal pag-button pag-button-desktop"
              onClick={() => this.goToPage(index)} key={index}>{item + 1}</button>
          )
        })}
      </div>
    )
  }

  render() {
    const { mobile, pageSize, page, recordsCount } = this.props;

    const pages = Math.ceil(recordsCount / pageSize);

    return (
      <div className={`pagination-area`}>
        <button
          className={`soft-text-normal pag-button ${mobile ? 'pag-button-mobile' : 'pag-button-desktop'}`}
          disabled={page === 0}
          onClick={() => this.goToPage(0)}>{'<<'}</button>
        <button
          className={`soft-text-normal pag-button ${mobile ? 'pag-button-mobile' : 'pag-button-desktop'}`}
          disabled={page === 0}
          onClick={() => this.previousPage()}>{'<'}</button>
        {!mobile
          ? this.getBlocks()
          : null}
        <button
          className={`soft-text-normal pag-button ${mobile ? 'pag-button-mobile' : 'pag-button-desktop'}`}
          disabled={page === pages - 1}
          onClick={() => this.nextPage()}>{'>'}</button>
        <button
          className={`soft-text-normal pag-button ${mobile ? 'pag-button-mobile' : 'pag-button-desktop'}`}
          disabled={page === pages - 1}
          onClick={() => this.goToPage(pages - 1)}>{'>>'}</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pageSize: state.responsible.pageSize,
    page: state.responsible.page,
    recordsCount: state.responsible.recordsCount,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    loadResponsibles: (size, page) => dispatch(loadResponsibles(size, page)),
    nextPage: () => dispatch(nextPage()),
    previousPage: () => dispatch(previousPage()),
    goToPage: (page) => dispatch(goToPage(page)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Pagination));
