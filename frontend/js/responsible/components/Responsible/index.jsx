import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import './index.scss';

class Responsible extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
    data: PropTypes.object,
    onDelete: PropTypes.func,
    onEdit: PropTypes.func,
  };

  static defaultProps = {
    mobile: false,
    onDelete: () => {
    },
    onEdit: () => {
    },
  };

  toCpf(text) {
    return `${text.slice(0, 3)}.${text.slice(3, 6)}.${text.slice(6, 9)}-${text.slice(9, 11)}`;
  }

  toDate(date) {
    const d = new Date(date);
    return `${d.getDate() + 1}/${d.getMonth() + 1}/${d.getFullYear()}`;
  }

  render() {
    const { mobile, data } = this.props;

    return (
      <div className={`responsible-item`}>
        <div className={'responsible-item-row'}>
          <div
            className={`responsible-item-info ${mobile ? 'responsible-item-info-mobile' : 'responsible-item-info-desktop'}`}>
            <div
              className={`responsible-item-text-bolder soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}>{data.nome}</div>
            {!mobile ?
              <div className={`responsible-item-text soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}> - </div> : null}
            <div className={`responsible-item-text soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}>{data.email}</div>
          </div>
          <div
            className={`responsible-item-info ${mobile ? 'responsible-item-info-mobile' : 'responsible-item-info-desktop'}`}>
            <div
              className={`responsible-item-text soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}>{this.toCpf(data.cpf)}</div>
            {!mobile ?
              <div className={`responsible-item-text soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}> - </div> : null}
            <div className={`responsible-item-text soft-text-small ${mobile ? 'mob-fs' : 'd-fs'}`}>
              Nascimento: {this.toDate(data.data_nascimento)}</div>
          </div>
        </div>
        <div className={'responsible-item-actions'}>
          <div onClick={() => this.props.onEdit(data.id)}>
            <FontAwesomeIcon
              color={'#333333'}
              size={!mobile ? '2x' : '5x'}
              icon="edit"
              className={'responsible-icon'}>!</FontAwesomeIcon>
          </div>
          <div onClick={() => this.props.onDelete()}>
            <FontAwesomeIcon
              color={'#333333'}
              size={!mobile ? '2x' : '5x'}
              icon="trash-alt"
              className={'responsible-icon'}>?</FontAwesomeIcon>
          </div>
        </div>
      </div>
    );
  }
}

export default Responsible;
