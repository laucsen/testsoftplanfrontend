import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';

import './index.scss';

class ResponsibleInputCpf extends Component {
  static propTypes = {
    mobile: PropTypes.bool,
    caption: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
  };

  render() {
    const { caption, value, onChange, mobile } = this.props;

    return (
      <div className={'responsible-detail-input-item'}>
        <div className={`soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}>{caption}</div>
        <div className={'responsible-detail-block-input-area'}>
          <InputMask
            mask="999.999.999-99"
            className={`responsible-detail-block-input soft-text-small ${mobile ? 'mob-fs': 'd-fs'}`}
            type="text"
            value={value}
            onChange={onChange} />
        </div>
      </div>
    );
  }
}

export default ResponsibleInputCpf;
