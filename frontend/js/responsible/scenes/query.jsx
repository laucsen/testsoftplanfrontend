import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import {
  loadResponsibles,
  deleteResponsible,
} from '../actions';

import Header from '../components/Header';
import Responsible from '../components/Responsible';
import Pagination from '../components/Pagination';
import ResponsibleInput from '../components/ResponsibleInput';
import ResponsibleInputCpf from '../components/ResponsibleInputCpf';

import './query.scss';

const MOBILE_WIDTH = 1224;

/**
 * Mains Skills screen.
 */
class Query extends Component {
  static propTypes = {
    records: PropTypes.array,
    recordsCount: PropTypes.number,
    loadResponsibles: PropTypes.func,
    deleteResponsible: PropTypes.func,
    pageSize: PropTypes.number,
    page: PropTypes.number,
    history: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      search: {
        nome: '',
        email: '',
        cpf: '',
      },
    };
  }

  componentDidMount() {
    const { pageSize, page } = this.props;
    this.props.loadResponsibles(pageSize, page);
  }

  getHeader(mobile) {
    return (
      <Header mobile={mobile} />
    )
  }

  deleteResponsible(responsible, index) {
    const { pageSize, page } = this.props;
    this
      .props
      .deleteResponsible(responsible, index)
      .then(() => this.props.loadResponsibles(pageSize, page));
  }

  editResponsible(id) {
    this.props.history.push(`/edit/${id}`);
  }

  changeSearch(attribute, value) {
    const { search = {} } = this.state;
    search[attribute] = value;
    this.setState({ search });
  }

  applySearch() {
    const { pageSize, page } = this.props;
    this.props.loadResponsibles(pageSize, page, this.state.search);
    this.setState({
      search: {
        nome: '',
        email: '',
        cpf: '',
      },
    });
  }

  // Get all screen elements for mobile or not.
  getElements(mobile) {
    const { records = [] } = this.props;
    const { search = {} } = this.state;
    const { nome, email, cpf } = search;

    return (
      <div style={{
        width: mobile ? '100%' : '70%',
        margin: '0px auto',
        border: '1px solid black',
      }}>
        <div>
          {this.getHeader(mobile)}
        </div>
        <div className={`responsible-query-area ${mobile ? 'responsible-query-area-mobile' : ''}`}>
          <div
            className={`soft-text-large responsible-query-title ${mobile ? 'responsible-query-title-mobile' : 'responsible-query-title-desktop'}`}>Consulta
            de Responsáveis
          </div>
          <div>
            <Link to={'/criar/'}>
              <FontAwesomeIcon
                color={'#333333'}
                size={!mobile ? '2x' : '5x'}
                icon="plus-square"
                className={'responsible-create'}>+</FontAwesomeIcon>
            </Link>
          </div>
        </div>

        <div className={`quering-area ${mobile ? 'quering-area-mobile' : 'quering-area-desktop'}`}>
          <div className={'quering-item'}>
            <ResponsibleInput
              value={nome}
              onChange={(ev) => this.changeSearch('nome', ev.target.value)}
              mobile={mobile}
              caption={'Nome'}
            />
          </div>
          <div className={'quering-item'}>
            <ResponsibleInput
              value={email}
              onChange={(ev) => this.changeSearch('email', ev.target.value)}
              mobile={mobile}
              caption={'E-mail'}
            />
          </div>
          <div className={'quering-item'}>
            <ResponsibleInputCpf
              value={cpf}
              onChange={(ev) => this.changeSearch('cpf', ev.target.value)}
              mobile={mobile}
              caption={'Cpf'}
            />
          </div>
          <div className={'quering-item'}>
            <button
              className={`soft-text-normal query-button ${mobile ? 'mob-fs' : 'd-fs'}`}
              onClick={() => this.applySearch()}>Consultar
            </button>
          </div>
        </div>

        <div className={'query-dashed-sep'} />

        <div>
          {records.map((item, index) => {
            return (
              <Responsible
                key={index}
                mobile={mobile}
                data={item}
                onDelete={() => this.deleteResponsible(item, index)}
                onEdit={(id) => this.editResponsible(id)}
              />
            )
          })}
        </div>

        <div>
          <Pagination mobile={mobile} />
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className={'responsible-scene'}>
        <MediaQuery query={`(min-device-width: ${MOBILE_WIDTH}px)`}>
          {this.getElements(false)}
        </MediaQuery>
        <MediaQuery query={`(max-device-width: ${MOBILE_WIDTH}px)`}>
          {this.getElements(true)}
        </MediaQuery>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    records: state.responsible.records,
    recordsCount: state.responsible.recordsCount,
    pageSize: state.responsible.pageSize,
    page: state.responsible.page,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    loadResponsibles: (size, page, search) => dispatch(loadResponsibles(size, page, search)),
    deleteResponsible: (responsible, index) => dispatch(deleteResponsible(responsible, index)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Query));
