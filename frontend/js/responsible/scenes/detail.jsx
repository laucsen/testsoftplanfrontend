import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import { withRouter } from 'react-router-dom';

import {
  createResponsible,
  loadSingleResponsible,
  editResponsible,
} from '../actions';

import Header from '../components/Header';
import ResponsibleInput from '../components/ResponsibleInput';
import ResponsibleInputCpf from '../components/ResponsibleInputCpf';
import ResponsibleInputDate from '../components/ResponsibleInputDate';

import './detail.scss';

const MOBILE_WIDTH = 1224;

/**
 * Mains Skills screen.
 */
class Detail extends Component {
  static propTypes = {
    records: PropTypes.array,
    recordsCount: PropTypes.number,
    creating: PropTypes.bool,
    createResponsible: PropTypes.func,
    editResponsible: PropTypes.func,
    loadSingleResponsible: PropTypes.func,
    responsible: PropTypes.object,
    match: PropTypes.object,
    history: PropTypes.object,
  };

  static defaultProps = {
    creating: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      id: -1,
      nome: '',
      email: '',
      cpf: '',
      birthDate: new Date(Date.now()),
    };
  }

  componentDidMount() {
    if (!this.props.creating) {
      const { id } = this.props.match.params;
      this
        .props
        .loadSingleResponsible(id);
    }
  }

  componentDidUpdate(prevProps = {}) {
    if (!prevProps.creating) {
      const { responsible } = this.props;
      if (responsible && this.state.id !== responsible.id) {
        this.setState(responsible);
      }
    }
  }

  getHeader(mobile) {
    return (
      <Header mobile={mobile} />
    )
  }

  changeValue(property, value) {
    const state = this.state;
    state[property] = value;
    this.setState(state);
  }

  create() {
    const { history } = this.props;
    this.props
      .createResponsible(this.state)
      .then(() => history.push('/'));
  }

  edit() {
    const { history } = this.props;
    this.props
      .editResponsible(this.state)
      .then(() => history.push('/'));
  }

  // Get all screen elements for mobile or not.
  getElements(mobile) {
    const { creating } = this.props;
    const { nome, email, cpf, birthDate } = this.state;

    return (
      <div style={{
        width: mobile ? '100%' : '70%',
        margin: '0px auto',
        border: '1px solid black'
      }}>
        <div>
          {this.getHeader(mobile)}
        </div>
        <div className={`responsible-detail-area ${mobile ? 'responsible-detail-area-mobile' : ''}`}>
          <div>
            {
              creating
                ? <div
                  className={`soft-text-large responsible-detail-title ${mobile ? 'responsible-detail-title-mobile' : 'responsible-detail-title-desktop'}`}>Cadastro
                  de Responsável
                </div>
                : <div
                  className={`soft-text-large responsible-detail-title ${mobile ? 'responsible-detail-title-mobile' : 'responsible-detail-title-desktop'}`}>Edição
                  de Responsável
                </div>}
          </div>
          <div className={'responsible-detail-block'}>
            <ResponsibleInput
              mobile={mobile}
              caption={'Nome'}
              value={nome}
              onChange={(ev) => this.changeValue('nome', ev.target.value)} />
            <ResponsibleInput
              mobile={mobile}
              caption={'E-mail'}
              value={email}
              onChange={(ev) => this.changeValue('email', ev.target.value)} />
          </div>
          <div className={'responsible-detail-block'}>
            <ResponsibleInputCpf
              mobile={mobile}
              caption={'Cpf'}
              value={cpf}
              onChange={(ev) => this.changeValue('cpf', ev.target.value)} />
            <ResponsibleInputDate
              mobile={mobile}
              caption={'Data de Nascimento'}
              value={birthDate}
              onChange={(date) => this.changeValue('birthDate', date)} />
          </div>
          <div className={'responsible-detail-block'}>
            {creating
              ?
              <button className={'responsible-button soft-text-small'} onClick={() => this.create()}>Cadastrar</button>
              : <button className={'responsible-button soft-text-small'} onClick={() => this.edit()}>Editar</button>}
          </div>
        </div>

      </div>
    )
  }

  render() {
    return (
      <div className={'responsible-scene'}>
        <MediaQuery query={`(min-device-width: ${MOBILE_WIDTH}px)`}>
          {this.getElements(false)}
        </MediaQuery>
        <MediaQuery query={`(max-device-width: ${MOBILE_WIDTH}px)`}>
          {this.getElements(true)}
        </MediaQuery>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    records: state.responsible.records,
    recordsCount: state.responsible.recordsCount,
    responsible: state.responsible.responsible,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    createResponsible: (data) => dispatch(createResponsible(data)),
    loadSingleResponsible: (id) => dispatch(loadSingleResponsible(id)),
    editResponsible: (data) => dispatch(editResponsible(data)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Detail));
