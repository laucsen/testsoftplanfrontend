import { createAction } from 'redux-actions';
import { toast } from 'react-toastify';

import API from '../components/API';

// Actions we dispatch in our Action Creators
export const loadResponsibleRequested = createAction('LOAD_RESPONSIBLE_REQUESTED');
export const loadResponsibleFailed = createAction('LOAD_RESPONSIBLE_FAILED');
export const loadResponsibleSucceeded = createAction('LOAD_RESPONSIBLE_SUCCEEDED');

export const createResponsibleRequested = createAction('LOAD_RESPONSIBLE_REQUESTED');
export const createResponsibleFailed = createAction('LOAD_RESPONSIBLE_FAILED');
export const createResponsibleSucceeded = createAction('LOAD_RESPONSIBLE_SUCCEEDED');

export const deleteResponsibleRequested = createAction('DELETE_RESPONSIBLE_REQUESTED');
export const deleteResponsibleFailed = createAction('DELETE_RESPONSIBLE_FAILED');
export const deleteResponsibleSucceeded = createAction('DELETE_RESPONSIBLE_SUCCEEDED');

export const getResponsibleRequested = createAction('GET_RESPONSIBLE_REQUESTED');
export const getResponsibleFailed = createAction('GET_RESPONSIBLE_FAILED');
export const getResponsibleSucceeded = createAction('GET_RESPONSIBLE_SUCCEEDED');

export const editResponsibleRequested = createAction('EDIT_RESPONSIBLE_REQUESTED');
export const editResponsibleFailed = createAction('EDIT_RESPONSIBLE_FAILED');
export const editResponsibleSucceeded = createAction('EDIT_RESPONSIBLE_SUCCEEDED');

export const nextPageAction = createAction('NEXT_PAGE');
export const previousPageAction = createAction('PREVIOUS_PAGE');
export const goToPageAction = createAction('GO_TO_PAGE');

export const loadResponsibles = (size = 10, page = 0, search = {}) => {
  return async (dispatch) => {
    dispatch(loadResponsibleRequested());
    const body = await API.post(`http://192.168.99.100:7070/api/responsaveis/find-all?size=${size}&page=${page}`, {
      body: search,
    });
    if (body.error) {
      return dispatch(loadResponsibleFailed({ error: body.error }));
    }
    dispatch(loadResponsibleSucceeded(body));
  };
};

export const createResponsible = (data) => {
  return async (dispatch) => {
    dispatch(createResponsibleRequested(data));
    const body = await API.post(`http://192.168.99.100:7070/api/responsaveis/`, {
      body: {
        nome: data.nome,
        email: data.email,
        data_nascimento: data.birthDate,
        cpf: data.cpf.replace(/[.-]/gm, ''),
      }
    });
    if (body.error) {
      body.error.forEach((err) => {
        toast.error(err.mensagem.replace('{}', err.args[0] || ''), {
          position: toast.POSITION.TOP,
        });
      });
      return dispatch(createResponsibleFailed({ error: body.error }));
    }
    dispatch(createResponsibleSucceeded(body));
  };
};

export const deleteResponsible = (data, index) => {
  return async (dispatch) => {
    dispatch(deleteResponsibleRequested(data, index));
    const body = await API.delete(`http://192.168.99.100:7070/api/responsaveis/${data.id}`);
    if (body.error) {
      return dispatch(deleteResponsibleFailed({ error: body.error }));
    }
    dispatch(deleteResponsibleSucceeded(body));
  };
};

export const loadSingleResponsible = (id) => {
  return async (dispatch) => {
    dispatch(getResponsibleRequested(id));
    const body = await API.get(`http://192.168.99.100:7070/api/responsaveis/${id}`);
    if (body.error) {
      return dispatch(getResponsibleFailed({ error: body.error }));
    }
    dispatch(getResponsibleSucceeded(body));
  };
};

export const editResponsible = (data) => {
  return async (dispatch) => {
    dispatch(editResponsibleRequested(data));
    const body = await API.put(`http://192.168.99.100:7070/api/responsaveis/${data.id}`, {
      body: data,
    });
    if (body.error) {
      return dispatch(editResponsibleFailed({ error: body.error }));
    }
    dispatch(editResponsibleSucceeded(body));
  };
};

export const nextPage = () => {
  return async (dispatch) => dispatch(nextPageAction());
};

export const previousPage = () => {
  return async (dispatch) => dispatch(previousPageAction());
};

export const goToPage = (page) => {
  return async (dispatch) => dispatch(goToPageAction(page));
};
