// Helpers
export const accessToken = ({auth}) => {
  if (auth.access) {
    return auth.access.token
  }
};

export const refreshToken = ({auth}) => {
  if (auth.refresh) {
    return auth.refresh.token
  }
};

export const isAccessTokenExpired = ({auth}) => {
  if (auth.access && auth.access.exp) {
    return 1000 * auth.access.exp - (new Date()).getTime() < 5000;
  }
  return true;
};

export const isRefreshTokenExpired = ({auth = {}}) => {
  if (auth.refresh && auth.refresh.exp) {
    return 1000 * auth.refresh.exp - (new Date()).getTime() < 5000;
  }
  return true;
};

export const isAuthenticated = (state) => {
  return !isRefreshTokenExpired(state);
};

export const authErrors = ({auth}) => {
  return auth.loginError;
};

/**
 * Atach auth token to headers.
 * @param headers
 * @returns {function(*=): {Authorization: string}}
 */
export const withAuth = (headers = {}) => {
  return (state) => {
    return {
      ...headers,
      'Authorization': `Bearer ${accessToken(state)}`
    }
  };
};
