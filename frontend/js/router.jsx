import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './app';

import ResponsibleQuery from './responsible/scenes/query';
import ResponsibleDetails from './responsible/scenes/detail';

import store from './create-store';

/**
 * Main router. We have only one route, but...
 */
class VaultTechRouter extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <App>
            <Switch>
              <Route exact path="/" component={ResponsibleQuery} />
              <Route exact path="/criar" component={() => (<ResponsibleDetails creating={true} />)} />
              <Route exact path="/edit/:id" component={() => (<ResponsibleDetails creating={false} />)} />
            </Switch>
          </App>
        </Router>
      </Provider>
    );
  }
}

export default VaultTechRouter;