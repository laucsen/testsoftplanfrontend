import React from 'react';
import ReactDOM from 'react-dom';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlusSquare, faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import VaultTechRouter from './router';

import 'babel-polyfill';

library.add(faPlusSquare);
library.add(faEdit);
library.add(faTrashAlt);

ReactDOM.render(
  <VaultTechRouter />,
  /* eslint-disable no-undef */
  document.getElementById('react-app')
);

if (module.hot) {
  module.hot.accept('./router.jsx', function () {
    const NextVaultTechRouter = require('./router').default;
    ReactDOM.render(
      <NextVaultTechRouter />,
      document.getElementById('react-app')
    );
  })
}
